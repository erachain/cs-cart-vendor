<a class="btn" onclick="get_seed();">{__("csc_generate_new_seed")}</a><br>
<span style="color: red">{__("change_seed_notification")}</span> <input type="checkbox" name="change_seed" value="Y"> {__("yes")}<hr>
<div id="seed"></div>

<script>
    function get_seed()
    {
        var url = '{'csc_erachain.get_seed'|fn_url}';
        $.ceAjax('request', url, {
            callback: function (data, params){
                $('#seed').html(data.seed);
            },
            method: 'get'
        });
    }
</script>