{if !$hide_title}
    {include file="common/subheader.tpl" title="Erachain" target="#erachain"}
{/if}
{if $runtime.company_id > 0}
    {assign var="csc_disabled" value="disabled"}
{/if}
<div id="erachain" class="collapsed in">
    <div class="control-group">
        <label class="control-label" for="erachain_account">{__("csc_erachain_address")}:</label>
        <div class="controls">
            <input type="text" name="company_data[erachain_address]" value="{$company_data.erachain_address}" class="input-large" {$csc_disabled}>
        </div>
    </div>

    {*
    {if $runtime.company_id == 0}
        <div class="control-group">
            <label class="control-label" for="erachain_account">{__("csc_erachain_seed")}:</label>
            <div class="controls">
                <input type="text" name="company_data[erachain_account_seed]" value="{$company_data.erachain_account_seed}" class="input-large">
            </div>
        </div>
    {/if}*}

    <div class="control-group">
        <label class="control-label" for="erachain_public_key">{__("csc_erachain_public_key")}:</label>
        <div class="controls">
            <input type="text" name="company_data[erachain_public_key]" value="{$company_data.erachain_public_key}" class="input-large" {$csc_disabled}>
        </div>
    </div>

    {if $runtime.company_id == 0}
        <div class="control-group">
            <label class="control-label" for="erachain_private_key">{__("csc_erachain_private_key")}:</label>
            <div class="controls">
                <input type="text" name="company_data[erachain_private_key]" value="{$company_data.erachain_private_key}" class="input-large">
            </div>
        </div>
    {/if}

    {if $runtime.company_id == 0}
        <input class="btn" type="submit" name="dispatch[csc_erachain.generate_company_account]" value="{__("csc_generate_new_account")}">
    {/if}
</div>
