{if !empty($oi.csc_erachain_signature)}
    <tr>
        <td colspan="4">
            <div style="word-break: break-all;"><b>Signature</b>: {$oi.csc_erachain_signature}</div>
        </td>
    </tr>
{/if}
{if !empty($oi.csc_erachain_seqNo)}
    <tr>
        <td colspan="4">
            <b>SeqNo</b>: {$oi.csc_erachain_seqNo}
        </td>
    </tr>
{/if}
{if !empty($oi.csc_erachain_response)}
    <tr>
        <td colspan="4">
            <b>{__("response")}</b>: {$oi.csc_erachain_response}
        </td>
    </tr>
{/if}