<?php
use Tygh\Registry;
use Erachain\Erachain;
use Tygh\Settings;

function fn_csc_erachain_get_product_data_pre($product_id, $auth, $lang_code, $field_list, $get_add_pairs, $get_main_pair, $get_taxes, $get_qty_discounts, $preview, $features, $skip_company_condition, $params)
{
    $amount = fn_csc_get_asset_amount($product_id);
    if (!empty($amount)) {
        db_query('update ?:products set amount = ?d where product_id = ?i', $amount, $product_id);
    }
}

function fn_csc_erachain_change_order_status_post($order_id, $status_to, $status_from, $force_notification, $place_order, $order_info, $edp_data)
{
    $addon = Registry::get('addons.csc_erachain');
    if ($status_to == $addon['status_processing']) {
        $era = new Erachain($addon['mode']);
        $order = db_get_array('select item_id, order_id, product_id, amount, csc_erachain_signature from ?:order_details where order_id = ?i and product_id in (select product_id from ?:products where not csc_erachain_asset_key = "")', $order_id);
        $products_assets = db_get_hash_array('select product_id, csc_erachain_asset_key from ?:products where not csc_erachain_asset_key = ""', 'product_id');

        $order_info = fn_get_order_info($order_id);
        $company_id = $order_info['company_id'];
        $incomplete = false;
        $waiting_for_info = false;
        $not_enough_compu = false;
        foreach ($order as $product) {
            if (!empty($products_assets[$product['product_id']])) {
                if (!empty($product['csc_erachain_signature'])) {
                    $seqno = db_get_field('select csc_erachain_seqNo from ?:order_details where item_id = ?i and order_id = ?i', $product['item_id'], $order_id);
                    if (empty($seqno)) {
                        $transaction = $era->api('/api/tx/' . $product['csc_erachain_signature']);
                        $result = json_decode($transaction['DATA'], 1);
                        if ($transaction['STATUS'] == "OK") {
                            if ($result['confirmations'] < $addon['transaction_attempts']) {
                                $incomplete = true;
                            }
                            db_query('update ?:order_details set csc_erachain_seqNo = ?s where item_id = ?i and order_id = ?i', $result['seqNo'], $product['item_id'], $order_id);
                            db_query('update ?:order_details set csc_erachain_response = "" where item_id = ?i and order_id = ?i', $product['item_id'], $order_id);
                            continue;
                        }
                        elseif ($transaction['STATUS'] == 'ERROR') {
                            $incomplete_orders []= $order_id;
                            if (!empty($transaction['DATA'])) {
                                db_query('update ?:order_details set csc_erachain_response = ?s where item_id = ?i and order_id = ?i', $transaction['DATA'], $product['item_id'], $order_id);
                            }
                            fn_change_order_status($order_id, $addon['status_transaction_error']);
                        }
                    }
                    else {
                        continue;
                    }
                }
                $incomplete = true;
                if (!$not_enough_compu) {
                    $compu = fn_csc_get_company_compu($company_id);
                    if ($compu < $addon['min_compu']) {
                        $email = db_get_field('select email from ?:companies where company_id = ?i', $company_id);
                        $mailer = Tygh::$app['mailer'];
                        $mailer->send(array(
                            'to' => $email,
                            'from' => 'default_company_newsletter_email',
                            'reply_to' => 'default_company_newsletter_email',
                            'data' => array(
                                'compu' => $compu,
                                'min_compu' => $addon['min_compu'],
                            ),
                            'tpl' => 'addons/csc_erachain/min_compu.tpl'
                        ), 'A');
                        $not_enough_compu = true;
                    }
                    elseif (!$waiting_for_info) {
                        $field_id = fn_csc_get_erachain_address_field_id();
                        if (!empty($order_info['fields'][$field_id])) {
                            $customer_erachain_address = $order_info['fields'][$field_id];
                        }
                        else {
                            $user_data = fn_get_user_info($order_info['user_id']);
                            if (!empty($user_data['fields'][$field_id])) {
                                $customer_erachain_address = $user_data['fields'][$field_id];
                            }
                            else {
                                fn_change_order_status($order_id, $addon['status_awaiting_for_address']);
                                $waiting_for_info = true;
                            }
                        }
                        if (!empty($customer_erachain_address)) {
                            $product['asset_key'] = $products_assets[$product['product_id']]['csc_erachain_asset_key'];
                            $signature = fn_csc_send_active($company_id, $customer_erachain_address, $product);
                            if ($signature['STATUS'] == 'OK' && !empty($signature['DATA']['signature'])) {
                                db_query('update ?:order_details set csc_erachain_signature = ?s where item_id = ?i and order_id = ?i', $signature['DATA']['signature'], $product['item_id'], $order_id);
                                db_query('update ?:order_details set csc_erachain_response = "" where item_id = ?i and order_id = ?i', $product['item_id'], $order_id);
                            }
                            else {
                                $incomplete = true;
                                if (!empty($signature['DATA'])) {
                                    db_query('update ?:order_details set csc_erachain_response = ?s where item_id = ?i and order_id = ?i', $signature['DATA'], $product['item_id'], $order_id);
                                }
                                fn_change_order_status($order_id, $addon['status_transaction_error']);
                            }
                        }
                    }
                }
            }
        }
        if (!$incomplete) {
            fn_change_order_status($order_id, $addon['status_completed']);
        }
    }
}

function fn_csc_erachain_update_company($company_data, $company_id, $lang_code, $action)
{
    fn_generate_company_erachain_account($company_id);
}

function fn_csc_erachain_settings_update_value_by_id_pre($that, $object_id, $value, $company_id, $execute_functions, &$data, $old_data, $table, $storefront_id)
{
    if (empty($_REQUEST['change_seed'])) {
        $addon = Settings::instance()->getSectionByName('csc_erachain', 'ADDON');
        $option = db_get_row('select object_id, value from ?:settings_objects where type = "I" and name = "seed" and section_id = ?i', $addon['section_id']);
        if ($data['object_id'] == $option['object_id']) {
            $data['value'] = $old_data['value'];
        }
    }
}