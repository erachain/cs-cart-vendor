<?php

fn_register_hooks(
    'get_product_data_pre',
    'change_order_status_post',
    'update_company',
    'settings_update_value_by_id_pre'
);