<?php
use Erachain\Erachain;

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if ($mode == 'update') {
        $field_id = fn_csc_get_erachain_address_field_id();
        if (isset($_REQUEST['user_data']['fields'][$field_id])) {
            $era = new Erachain('dev');
            $validation = $era->api('/api/addressvalidate/' . $_REQUEST['user_data']['fields'][$field_id], false, 'get');
            if ($validation['DATA'] == 'false') {
                fn_set_notification('E', __("error"), __("csc_erachain_address_invalid"));
                $_REQUEST['user_data']['fields'][$field_id] = '';
            }
        }
    }
}