<?php
use Tygh\Registry;
use Erachain\Erachain;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

$addon = Registry::get('addons.csc_erachain');
if ($_REQUEST['cron_key'] != $addon['cron_key']) { die('Access denied'); }

if ($mode == 'check_servers') {
    db_query('delete from ?:csc_banned_node_servers where ban_expire <= ?i', time());
}

if ($mode == 'check_orders') {
    $addon = Registry::get('addons.csc_erachain');
    $era = new Erachain($addon['mode']);
    $order_ids = db_get_fields('select order_id from ?:orders where status in (?a)', [$addon['status_processing'], $addon['status_awaiting_for_address']]);
    $orders = db_get_hash_multi_array('select item_id, order_id, product_id, amount, csc_erachain_signature from ?:order_details where order_id in (?n) and product_id in (select product_id from ?:products where not csc_erachain_asset_key = "")', ['order_id'], $order_ids);
    $products_assets = db_get_hash_array('select product_id, csc_erachain_asset_key from ?:products where not csc_erachain_asset_key = ""', 'product_id');
    $not_enough_compu = []; //vendors
    $waiting_for_customer_address = [];
    $incomplete_orders = [];
    foreach ($orders as $order_id => $order) {
        if (in_array($order_id, $waiting_for_customer_address)) {
            continue;
        }
        $order_info = fn_get_order_info($order_id);
        $company_id = $order_info['company_id'];
        if (in_array($company_id, $not_enough_compu)) {
            continue;
        }

        foreach ($order as $product) {
            if (!empty($products_assets[$product['product_id']])) {
                if (!empty($product['csc_erachain_signature'])) {
                    $seqno = db_get_field('select csc_erachain_seqNo from ?:order_details where item_id = ?i and order_id = ?i', $product['item_id'], $order_id);
                    if (empty($seqno)) {
                        $transaction = $era->api('/api/tx/' . $product['csc_erachain_signature']);
                        $result = json_decode($transaction['DATA'], 1);
                        if ($transaction['STATUS'] == "OK" && !empty($result['seqNo'])) {
                            if ($result['confirmations'] < $addon['transaction_attempts']) {
                                $incomplete_orders []= $order_id;
                            }
                            db_query('update ?:order_details set csc_erachain_seqNo = ?s where item_id = ?i and order_id = ?i', $result['seqNo'], $product['item_id'], $order_id);
                            db_query('update ?:order_details set csc_erachain_response = "" where item_id = ?i and order_id = ?i', $product['item_id'], $order_id);
                            continue;
                        }
                        elseif ($transaction['STATUS'] == 'ERROR') {
                            if (!empty($transaction['DATA'])) {
                                db_query('update ?:order_details set csc_erachain_response = ?s where item_id = ?i and order_id = ?i', $transaction['DATA'], $product['item_id'], $order_id);
                            }
                            $incomplete_orders []= $order_id;
                            fn_change_order_status($order_id, $addon['status_transaction_error']);
                        }
                    }
                    else {
                        continue;
                    }
                }
                $incomplete_orders []= $order_id;
                if (!in_array($company_id, $not_enough_compu)) {
                    $compu = fn_csc_get_company_compu($company_id);
                    if ($compu < $addon['min_compu']) {
                        $email = db_get_field('select email from ?:companies where company_id = ?i', $company_id);
                        $mailer = Tygh::$app['mailer'];
                        $mailer->send(array(
                            'to' => $email,
                            'from' => 'default_company_newsletter_email',
                            'reply_to' => 'default_company_newsletter_email',
                            'data' => array(
                                'compu' => $compu,
                                'min_compu' => $addon['min_compu'],
                            ),
                            'tpl' => 'addons/csc_erachain/min_compu.tpl'
                        ), 'A');
                        $not_enough_compu []= $company_id;
                        break;
                    }
                    else {
                        $field_id = fn_csc_get_erachain_address_field_id();
                        if (!empty($order_info['fields'][$field_id])) {
                            $customer_erachain_address = $order_info['fields'][$field_id];
                        }
                        else {
                            $user_data = fn_get_user_info($order_info['user_id']);
                            if (!empty($user_data['fields'][$field_id])) {
                                $customer_erachain_address = $user_data['fields'][$field_id];
                            }
                            else {
                                fn_change_order_status($order_id, $addon['status_awaiting_for_address']);
                                $waiting_for_customer_address []= $order_id;
                                break;
                            }
                        }
                        if (!empty($customer_erachain_address)) {
                            $product['asset_key'] = $products_assets[$product['product_id']]['csc_erachain_asset_key'];
                            $signature = fn_csc_send_active($company_id, $customer_erachain_address, $product);
                            if ($signature['STATUS'] == 'OK' && !empty($signature['DATA']['signature'])) {
                                db_query('update ?:order_details set csc_erachain_signature = ?s where item_id = ?i and order_id = ?i', $signature['DATA']['signature'], $product['item_id'], $order_id);
                                db_query('update ?:order_details set csc_erachain_response = "" where item_id = ?i and order_id = ?i', $product['item_id'], $order_id);
                            }
                            else {
                                $incomplete_orders []= $order_id;
                                if (!empty($signature['DATA'])) {
                                    db_query('update ?:order_details set csc_erachain_response = ?s where item_id = ?i and order_id = ?i', $signature['DATA'], $product['item_id'], $order_id);
                                }
                                fn_change_order_status($order_id, $addon['status_transaction_error']);
                            }
                        }
                    }
                }
            }
        }
        $complete_order_ids = array_diff(array_keys($orders), $incomplete_orders);
        foreach ($complete_order_ids as $order_id) {
            fn_change_order_status($order_id, $addon['status_completed']);
        }
    }
}

die('nice');