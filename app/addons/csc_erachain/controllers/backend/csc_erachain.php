<?php
use Erachain\Erachain;
use Tygh\Registry;
require ERACHAIN_SDK_PATH . '/autoload.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
    if ($mode == 'generate_company_account') {
        fn_generate_company_erachain_account($_REQUEST['company_id']);

        return array(CONTROLLER_STATUS_OK, 'companies.update?selected_section=addons&company_id=' . $_REQUEST['company_id']);
    }
}
else {
    if ($mode == 'get_seed') {
        $erachain = new Erachain(Registry::get('addons.csc_erachain.mode'));
        $seed = $erachain->crypto->generate_seed();
        Tygh::$app['ajax']->assign('seed', $seed['DATA']['seed_base58']);
    }
}