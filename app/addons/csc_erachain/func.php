<?php
use Erachain\Erachain;
use Tygh\Registry;

require_once (Registry::get('config.dir.addons') . 'csc_erachain/config.php');
require ERACHAIN_SDK_PATH . '/autoload.php';
require 'hooks.php';

function fn_csc_erachain_get_seed()
{
    $form = Tygh::$app['view']->fetch('addons/csc_erachain/components/get_seed.tpl');

    return $form;
}

function fn_csc_get_asset_amount($product_id)
{
    $addon = Registry::get('addons.csc_erachain');
    $erachain = new Erachain($addon['mode']);

    $asset_key = db_get_field('select csc_erachain_asset_key from ?:products where product_id = ?i', $product_id);
    $address = db_get_field('select erachain_address from ?:companies where company_id = (select company_id from ?:products where product_id = ?i)', $product_id);
    $params = [
        'address' => $address,
        'asset_key' => $asset_key
    ];
    $asset = $erachain->asset->api('addressassetbalance', $params);
    $asset = json_decode($asset['DATA'], 1);

    return $asset[0][1];
}

function fn_csc_get_erachain_address_field_id()
{
    return db_get_field('select field_id from ?:profile_fields where field_name = ?s', ERACHAIN_FIELD_NAME);
}

function fn_csc_get_company_compu($company_id)
{
    $addon = Registry::get('addons.csc_erachain');
    $era = new Erachain($addon['mode']);
    $address = db_get_field('select erachain_address from ?:companies where company_id = ?i', $company_id);
    $assets = $era->api('/api/addressassetbalance/' . $address . '/' . COMPU_ASSET_KEY);
    $assets = json_decode($assets['DATA']);

    return $assets[0][1];
}

function fn_csc_send_active($company_id, $address, $product)
{
    $addon = Registry::get('addons.csc_erachain');
    $era = new Erachain($addon['mode']);
    $company_data = db_get_row('select erachain_public_key, erachain_private_key from ?:companies where company_id = ?i', $company_id);

    $public_key = $company_data['erachain_public_key'];
    $private_key = $company_data['erachain_private_key'];

    $params = array(
        'recipient' => $address, // (string)
        'asset_key' => $product['asset_key'], // (int)
        'amount'    => $product['amount'], // (string)
        'head'      => 'Отправка актива покупателю', // (string)
        'message'   => 'Отправка актива покупателю', // (string)
        'encrypted' => $addon['encrypt_messages'] == "Y" ? 1 : 0, // (int)
        'is_text'   => 1 // (int)
    );
    $send_asset = $era->asset->send($public_key, $private_key, $params);
    return $send_asset;
}

function fn_generate_company_erachain_account($company_id)
{
    $addon = Registry::get('addons.csc_erachain');
    $erachain = new Erachain($addon['mode']);
    $account = $erachain->crypto->generate_account($addon['seed'], $company_id);

    $data = [
        'erachain_account_seed' => $account['DATA']['accountSeed'],
        'erachain_private_key' => $account['DATA']['private_key'],
        'erachain_public_key' => $account['DATA']['public_key'],
        'erachain_address' => $account['DATA']['address'],
    ];
    db_query('update ?:companies set ?u where company_id = ?i', $data, $company_id);

    return true;
}

function fn_csc_erachain_cron_info()
{
    $config = Registry::get('config');
    $cron_key = Registry::get('addons.csc_erachain.cron_key');
    return __("csc_cron_check_orders") . ": /usr/bin/wget -O - -q -t 1 'https://{$config['https_host']}{$config['https_path']}/?dispatch=csc_cron_jobs.check_orders&cron_key={$cron_key}'<br><br>
    " . __("csc_cron_unban_servers") . ": /usr/bin/wget -O - -q -t 1 'https://{$config['https_host']}{$config['https_path']}/?dispatch=csc_cron_jobs.check_servers&cron_key={$cron_key}'";
}