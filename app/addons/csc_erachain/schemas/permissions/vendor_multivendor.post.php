<?php

$schema['controllers']['csc_erachain'] = [
    'modes' => [
        'generate_company_account' => [
            'permissions' => false
        ],
        'get_seed' => [
            'permissions' => false
        ],
    ],
    'permissions' => true
];

return $schema;