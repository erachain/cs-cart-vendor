<?php

function fn_csc_erachain_get_statuses()
{
    $_statuses = db_get_array('select s.status, d.description from ?:statuses s inner join ?:status_descriptions d on s.status_id = d.status_id and s.type = "O"');
    $statuses = [];
    foreach($_statuses as $status) {
        $statuses[$status['status']] = $status['description'];
    }

    return $statuses;
}
function fn_settings_variants_addons_csc_erachain_status_awaiting_for_address()
{
    return fn_csc_erachain_get_statuses();
}

function fn_settings_variants_addons_csc_erachain_status_processing()
{
    return fn_csc_erachain_get_statuses();
}

function fn_settings_variants_addons_csc_erachain_status_completed()
{
    return fn_csc_erachain_get_statuses();
}

function fn_settings_variants_addons_csc_erachain_status_transaction_error()
{
    return fn_csc_erachain_get_statuses();
}